 #include"i2c.h"

void Init_i2c(void)
{

	       GPIO_InitTypeDef gpio_i2c;

	       // �������� ������������ ������ �������
	       RCC_APB2PeriphClockCmd(i2c_RCC_PORT,        ENABLE); // ������������ �����
           RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE); // ������������ �������������� �������
           RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE); // ������������ I2C1

	       // I2C ���������� ��� ���� ����������������, �� ���� ����� ���������
	       gpio_i2c.GPIO_Pin = i2c_Pin_SDA | i2c_Pin_SCK;
	       gpio_i2c.GPIO_Mode = GPIO_Mode_AF_OD;
	       gpio_i2c.GPIO_Speed = GPIO_Speed_10MHz;
	       GPIO_Init(i2c_Port, &gpio_i2c);

	                                   // ������� �������� ������� PCLK1 (����� ���� �� 2 �� 32 ���, ������������)
	       I2C1->CR2 &= ~I2C_CR2_FREQ; // �������� ���� ��������
	       I2C1->CR2 |= 6;             // ������������� ��������� (������� ������� PCLK1 -3���)
	     //  I2C1->CR2 |= I2C_CR2_DMAEN;  // ����� DMA
	       I2C1->CCR &= ~I2C_CCR_CCR;  // ���������� ��������������� �������� � ������������ � ��������� ��������� ������ 6 ��� / 400 ��� = 15;
	       I2C1->CCR |= I2C_CCR_FS;    // ������� �����
	       I2C1->CCR |=  5;            // ������ �������� ������ 400kH
	      // I2C1->CCR |= I2C_CCR_DUTY;  // ���������� � ������� ������  Fast Mode tLow/tHigh = 2 ��� 0(Fast Mode tLow/tHigh = 16/9)


	       I2C1->TRISE = 7;            // ������ �������� ������� ����� (1 / 6 ��� = 166 ��), ������������� ����� ������������ ����������:
	                                   // 1000 �� / 167 �� = 6 + 1 (���� ������� � ��������� �����) = 7
	       I2C1->CR1 |= I2C_CR1_PE;    // ��������� ������

}

u8 i2c_WriteBite_er(u8 addr_mpu, u8 addr_reg, u8 data) // ��������:  addr_mpu(����� ��������); addr_reg�����(����� ��������); data(���� ������).
{
	u32 count=0;

		       I2C1->CR1 |= I2C_CR1_START;            // ������������ ������� �����
		                                              // ���� ��������� ������������ ������� "�����"
		       while (!(I2C1->SR1 & I2C_SR1_SB)){count++;if(count==0xFFFF)return 4;}    // SB - ��������, ����� ������������ �����-�������,
		       (void) I2C1->SR1;                      // ����� ��������� ���������� ��������� ������� SR1, ��� ������ ���� SB.

		       I2C1->DR = addr_mpu<<1 ;               // �������� ����� �������� ���������� (MPU6050)

		       count=0;
		       while (!(I2C1->SR1 & I2C_SR1_ADDR)){count++;if(count==0xFFFF)return 4;}  // ������� ��������� �������� ������ (������� EV6), ���������������� ��� ADDR
		       (void) I2C1->SR1;                      // ���������� ��� ADDR (������� SR1 � SR2):
		       (void) I2C1->SR2;
		       if(I2C1->SR1 & I2C_SR1_AF)  {I2C1->CR1 |= I2C_CR1_STOP; return 1;} // ������, ��� ���� ACK ,
		       if(I2C1->SR1 & I2C_SR1_ARLO){return 2;} // ������, ������ ���� �� ����

		       count=0;
		       I2C1->DR = addr_reg;                   // �������� ����� ��������
		       while (!(I2C1->SR1 & I2C_SR1_TXE)){count++;if(count==0xFFFF)return 4;}   // ������� ��������� �������� ������
		       if(I2C1->SR1 & I2C_SR1_AF)  {I2C1->CR1 |= I2C_CR1_STOP; return 1;} // ������, ��� ���� ACK
		       if(I2C1->SR1 & I2C_SR1_ARLO){return 2;} // ������, ������ ���� �� ����

		       count=0;
		       I2C1->DR = data;                       // �������� ���� ������
		       while (!(I2C1->SR1 & I2C_SR1_TXE)){count++;if(count==0xFFFF)return 4;}   // ������� ��������� �������� ������
		       if(I2C1->SR1 & I2C_SR1_AF)  {I2C1->CR1 |= I2C_CR1_STOP; return 1;} // ������, ��� ���� ACK
		       if(I2C1->SR1 & I2C_SR1_ARLO){return 2;} // ������, ������ ���� �� ����

		   	   I2C1->CR1 |= I2C_CR1_STOP;             // ������������ ������� "����"

		   	   return 0;
}

u16  i2c_ReadBite_er(u8 addr_mpu, u8 addr_reg)                 // ������ ����� ������ �� ��� ������ �� ����, � ������ �������� � ������ ���������� �����(������� ����-������, ������� - ������)
{
	               u16 count=0;                             // ������� ��������
	               I2C1->CR1 |= I2C_CR1_ACK;                // �������� ACK
			       I2C1->CR1 |= I2C_CR1_START;              // ������������ ������� �����
			                                                // ���� ��������� ������������ ������� "�����"
			       while (!(I2C1->SR1 & I2C_SR1_SB)){count++; if(count==0xFFFF)return 4<<8;}      // SB - ��������, ����� ������������ �����-�������,
			       (void) I2C1->SR1;                        // ����� ��������� ���������� ��������� ������� SR1, ��� ������ ���� SB.
			       if(I2C1->SR1 & I2C_SR1_AF)  {I2C1->CR1 |= I2C_CR1_STOP; return 1<<8;} // ������, ��� ���� ACK ,
			       if(I2C1->SR1 & I2C_SR1_ARLO){return 2<<8;} // ������, ������ ���� �� ����

			       count=0;
			       I2C1->DR = addr_mpu<<1 ;                 // �������� ����� �������� ���������� (MPU6050) + 0
			       while (!(I2C1->SR1 & I2C_SR1_ADDR)){count++;if(count==0xFFFF)return 4<<8;}    // ������� ��������� �������� ������ (������� EV6), ���������������� ��� ADDR
			       (void) I2C1->SR1;                        // ���������� ��� ADDR (������� SR1 � SR2):
			       (void) I2C1->SR2;
			       if(I2C1->SR1 & I2C_SR1_AF)  {I2C1->CR1 |= I2C_CR1_STOP; return 1<<8;} // ������, ��� ���� ACK ,
			       if(I2C1->SR1 & I2C_SR1_ARLO){return 2<<8;} // ������, ������ ���� �� ����

			       count=0;
			       I2C1->DR = addr_reg;                     // �������� ����� ��������
			       while (!(I2C1->SR1 & I2C_SR1_TXE)){count++;if(count==0xFFFF)return 4<<8;}     // ������� ��������� �������� ������
			       if(I2C1->SR1 & I2C_SR1_AF)  {I2C1->CR1 |= I2C_CR1_STOP; return 1<<8;} // ������, ��� ���� ACK ,
			       if(I2C1->SR1 & I2C_SR1_ARLO){return 2<<8;} // ������, ������ ���� �� ����

			       count=0;
			       I2C1->CR1 |= I2C_CR1_START;              // ������������ ������� "��������� �����"
			       while (!(I2C1->SR1 & I2C_SR1_SB)){count++;if(count==0xFFFF)return 4<<8;}      // ���� ��������� ������������ ������� "��������� �����"
			       (void) I2C1->SR1;                        // ������� ���� ������

			       count=0;
			       I2C1->DR = (addr_mpu<<1)|1;              // �������� ����� �������� ���������� (MPU6050) +1(������)
			       while (!(I2C1->SR1 & I2C_SR1_ADDR)){count++;if(count==0xFFFF)return 4<<8;}    // ������� ��������� �������� ������
			       (void) I2C1->SR1;                        // ���������� ���� ADDR
			       (void) I2C1->SR2;
			       I2C1->CR1 &= ~I2C_CR1_ACK;               // �������� NACK ����� ���� (��������� ACK)
			       I2C1->CR1 |= I2C_CR1_STOP;               // ������������ ������� "����"

			       count=0;
			       while (!(I2C1->SR1 & I2C_SR1_RXNE)){count++;if(count==0xFFFF)return 4<<8;}    // ���� ������ �����
			       u8 data = I2C1->DR;                      // ������ �������� ����
	return data;
}

void i2c_WriteBite_Array(u8 addr_mpu, u8 addr_reg, u8 *data, u16 count ) //����� ��������, ����� ���������� ��������, ����� ������, ���������� ���� � ������
{
	           u16 Temp = 0;

		       I2C1->CR1 |= I2C_CR1_START;            // ������������ ������� �����
		                                              // ���� ��������� ������������ ������� "�����"
		       while (!(I2C1->SR1 & I2C_SR1_SB)){}    // SB - ��������, ����� ������������ �����-�������,
		       (void) I2C1->SR1;                      // ����� ��������� ���������� ��������� ������� SR1, ��� ������ ���� SB.

		       I2C1->DR = addr_mpu<<1 ;               // �������� ����� �������� ���������� (MPU6050)

		       while (!(I2C1->SR1 & I2C_SR1_ADDR)){}  // ������� ��������� �������� ������ (������� EV6), ���������������� ��� ADDR
		       (void) I2C1->SR1;                      // ���������� ��� ADDR (������� SR1 � SR2):
		       (void) I2C1->SR2;

		       I2C1->DR = addr_reg;                   // �������� ����� ��������
		       while (!(I2C1->SR1 & I2C_SR1_TXE)){}   // ������� ��������� �������� ������

		       for(; count>Temp; Temp++)
		       {
		       I2C1->DR = data[Temp];                 // �������� ���� ������
		       while (!(I2C1->SR1 & I2C_SR1_TXE)){}   // ������� ��������� �������� ������
		       }

		   	   I2C1->CR1 |= I2C_CR1_STOP;             // ������������ ������� "����"
}

void i2c_ReadBite_Array(u8 addr_mpu, u8 addr_reg,u8 *mas, u16 count)    // ������ ������ ����
{
	               //u8 data[count];
	               I2C1->CR1 |= I2C_CR1_ACK;                // �������� ACK
			       I2C1->CR1 |= I2C_CR1_START;              // ������������ ������� �����
			                                                // ���� ��������� ������������ ������� "�����"
			       while (!(I2C1->SR1 & I2C_SR1_SB)){}      // SB - ��������, ����� ������������ �����-�������,
			       (void) I2C1->SR1;                        // ����� ��������� ���������� ��������� ������� SR1, ��� ������ ���� SB.

			       I2C1->DR = addr_mpu<<1 ;                 // �������� ����� �������� ���������� (MPU6050) + 0
			       while (!(I2C1->SR1 & I2C_SR1_ADDR)){}    // ������� ��������� �������� ������ (������� EV6), ���������������� ��� ADDR
			       (void) I2C1->SR1;                        // ���������� ��� ADDR (������� SR1 � SR2):
			       (void) I2C1->SR2;

			       I2C1->DR = addr_reg;                     // �������� ����� ��������
			       while (!(I2C1->SR1 & I2C_SR1_BTF)){}     // ������� ��������� �������� ������

			       I2C1->CR1 |= I2C_CR1_START;              // ������������ ������� "��������� �����"
			       while (!(I2C1->SR1 & I2C_SR1_SB)){}      // ���� ��������� ������������ ������� "��������� �����"
			       (void) I2C1->SR1;                        // ������� ���� ������

			       I2C1->DR = (addr_mpu<<1)|1;              // �������� ����� �������� ���������� (MPU6050) +1(������)
			       while (!(I2C1->SR1 & I2C_SR1_ADDR)){}    // ������� ��������� �������� ������
			       (void) I2C1->SR1;                        // ���������� ���� ADDR
			       (void) I2C1->SR2;

			       u16 Temp = count - 1;
			       u16 Temp2 = 0;

			       for(;count>Temp2; Temp2++)               // ����� ������� �������������� �����, ������ ��������� ACK � ��������� STOP ���.
			       {
			    	     if(Temp2 == Temp)
			    	     {
			    	     I2C1->CR1 &= ~I2C_CR1_ACK;          // ��������� ACK (��� NACK)
			    	     I2C1->CR1 |= I2C_CR1_STOP;          // ������������ ������� "����"
			    	     }
			    	   while (!(I2C1->SR1 & I2C_SR1_RXNE)){} // ���� ������ �����
			    	   mas[Temp2] = I2C1->DR;                // ������ �������� ����
			       }
}

u8 i2c_WriteBite_check(u8 mpu, u8 reg, u8 DATA) // �������� ����, � ����������� ��������� , ���� �������� ������ �� ���������� 0. ����� 1
{
	u8 ERROR_TRANSFER = 0;
	u16 DATA_READ      = 0;
	ERROR_TRANSFER =  i2c_WriteBite_er(mpu ,reg ,DATA);
	DATA_READ      =  i2c_ReadBite_er(mpu,reg);
	if((DATA_READ>>8) != 0) return (DATA_READ>>8); // ���������� ������ ��� ������

    if(DATA_READ != DATA) return 3;                // ��������� ���������� ����
    return ERROR_TRANSFER;                         // ���������� ������ ��� ������

    // 0 - �������
    // 1 - ������, ���� ACK
    // 2 - ������, ������ ����� �� ���� ������
    // 3 - ������, �� ��������� ����
    // 4 - ������, ���������� �� ��������

}




