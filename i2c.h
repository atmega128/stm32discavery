#ifndef __I2C_H
#define __I2C_H

#include<stm32f10x_rcc.h>
#include<stm32f10x_gpio.h>
#include<stm32f10x_i2c.h>

#define i2c_Pin_SDA     GPIO_Pin_7           // ����� SDA ����������� i2c1
#define i2c_Pin_SCK     GPIO_Pin_6           // ����� SCK ����������� i2c1
#define i2c_Port        GPIOB
#define i2c_RCC_PORT    RCC_APB2Periph_GPIOB // ������������ �����

    // ����� ������:
    // 0 - �������
    // 1 - ������, ���� ACK
    // 2 - ������, ������ ����� �� ���� ������
    // 3 - ������, �� ��������� ����
    // 4 - ������, ���������� �� ��������

void Init_i2c(void);
u8   i2c_WriteBite_er(u8 addr_mpu, u8 addr_reg, u8 data);                 // (������ ����� � ����������) ����� ��������, ����� ��������, ������.// ���������� ������
u16  i2c_ReadBite_er(u8 addr_mpu, u8 addr_reg);                           // ������ ����� ������ �� ��� ������ �� ����, � ������ �������� � ������ ���������� �����(������� ����-������, ������� - ������)
u8   i2c_WriteBite_check(u8 addr_mpu, u8 addr_reg, u8 data);              // �������� ����, � ����������� ��������� ������ // ���������� ������
void i2c_WriteBite_Array(u8 addr_mpu, u8 addr_reg, u8 *data, u16 count ); // (������ ������ ����) ����� ����������, ����� ���������� ��������, ����� ������, ���������� ���� � ������.
void i2c_ReadBite_Array(u8 addr_mpu, u8 addr_reg,u8 *mas, u16 count) ;    // (������ ������ ����) ����� ����������, ����� ���������� ��������, ����� ���� ����� ����������� ������, ���������� ���� ��� ������.

#endif
