#ifndef __ENCODER_H
#define __ENCODER_H

#include<stm32f10x_rcc.h>
#include<stm32f10x_gpio.h>

// ��������� ���� ������� ��������� ������ ��� TIMER2,
#define ENCODER_PIN_A  GPIO_Pin_0
#define ENCODER_PIN_B  GPIO_Pin_1
#define ENCODER_PORT   GPIOA
#define ENCODER_RCC_PORT    RCC_APB2Periph_GPIOA

void EncodeInit(void);
uint16_t EncoderRead(void);        // ������ �������� ��������, 0...1000
void EncoderWrite(uint16_t count); // ������ �������� � ������� �������� 0...1000

#endif
