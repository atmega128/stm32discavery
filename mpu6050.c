 #include"mpu6050.h"
 #include"i2c.h"

u8  STATUS_TRANSFER =0; // ���� ���������� ������ ���� ����:
    // 0 - �������
    // 1 - ������, ���� ACK
    // 2 - ������, ������ ����� �� ���� ������
    // 3 - ������, �� ��������� ����
    // 4 - ������, ���������� �� ��������
    // 5 - ������, mpu6050 �� ���� �� ���������


u8 mpu6050Init(void) // ������������ ��� �������� �����, � ����������� �� ���������� ������
{
u16 mpu =  i2c_ReadBite_er(MPU6050,  MPU6050_WHO_AM_I ); // ��������� ������� mpu6050 �� ����
if((mpu>>8) != 0) return (mpu>>8); // ���������� ������ ������/������
if(mpu != 0x68) return 5;          // ���������� ������ �� ���������� ������


//������ ��������� RESET.
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_PWR_MGMT_1 ,DEVICE_RESET );
//Sets sample rate to 8000/1+7 = 1000Hz.
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_SMPLRT_DIV, 0x07 );
//������� ������������� ���������, DLPF(���) ���������.
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_CONFIG, EXT_SYNC_DISABLED | DLPF_BW_256  );
//�������� ��������� ��������� 250��/���
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_GYRO_CONFIG, FS_SEL_250 );
//�������� ��������������� ������������� 4g
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_ACCEL_CONFIG, ACCEL_FS_2 );
//���������� ��������� �������� ���������� �� ������� INT
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_INT_PIN_CFG  , INT_LEVEL_UP | INT_OPEN_PP |LATCH_INT_50mks | INT_RD_CLEAR_ALL |FSYNC_INT_DISABLE|I2C_BYPASS_DISABLE );

// ��������� �� ������ �������
// ����� ����������� ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_MOT_THR , 0x00 );
//����� FIFI ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_FIFO_EN, FIFO_DISABLE  );
// ��������������, ���� � MPU6050 ���������� ������� ��� �������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_FIFO_EN, 0x00  );
// ��������� �������� ���������� 0 - ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_I2C_SLV0_CTRL, I2C_SLV_DISABLE   );
// ��������� �������� ���������� 1 - ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_I2C_SLV1_CTRL, I2C_SLV_DISABLE   );
// ��������� �������� ���������� 2 - ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_I2C_SLV2_CTRL, I2C_SLV_DISABLE   );
// ��������� �������� ���������� 3 - ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_I2C_SLV3_CTRL, I2C_SLV_DISABLE   );
// ��������� �������� ���������� 0 - ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_I2C_SLV4_CTRL, I2C_SLV_DISABLE   );
//������ ������� ���������� ��������� i2c ����� master, ���������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_I2C_MST_STATUS , MST_I2C_DISABLE );
// �������� �������� i2c master
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050, MPU6050_I2C_MST_DELAY_CTRL ,0x00  );
//�������� ��� �������������� ��������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050, MPU6050_MOT_DETECT_CTRL  , 0x00);
// ���������������� ���������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_USER_CTRL , 0x00);
// ������� ���������� �������� � ������������ 2
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050, MPU6050_PWR_MGMT_2 , 0x00);



// ���������� ��� ������� ��������
STATUS_TRANSFER =   i2c_WriteBite_er(MPU6050, MPU6050_SIGNAL_PATH_RESET , GYRO_RESET_BIT | ACCEL_RESET_BIT | TEMP_RESET_BIT);
STATUS_TRANSFER =   i2c_WriteBite_er(MPU6050, MPU6050_SIGNAL_PATH_RESET , 0x00);
// ��������� ���������� �� ���������� ������
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050,  MPU6050_INT_ENABLE , DATA_RDY_EN  );
// ������� ���������� �������� � ������������ 1
STATUS_TRANSFER =   i2c_WriteBite_check(MPU6050, MPU6050_PWR_MGMT_1 , CLKSEL_PLL_Y); // ������������� �������

return STATUS_TRANSFER;
}
/*
void READING_MPU6050(u8 mas)
{
	i2c_ReadBite_Array(MPU6050, 0x3B, *mas, 14);
}


*/





