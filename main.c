/*
��������� - STM32Discavery
����������:
- LCD nokia5110
- encoder � �������
- mpu6050
������ ������� � CooCox
������ ������� ��� ������� ���������
PB7 - DATA i2c mpu6050
PB6 - CLK  i2c mpu6050
PB5 -      ADO mpu6050
PB2 -      INT mpu6050
PA0 - ENCODER A
PA1 - ENCODER B
PB1 - BUTTON ENCODER
PA4 - RESET    LCD
PA5 - SPI_SCL  LCD
PA6 - D/C      LCD
PA7 - SPI_MOSI LCD
PA3 - OPTO sensor
 */

#include "stm32f10x_conf.h"
#include "stm32f10x.h"
#include <stm32f10x_rcc.h>
#include <stm32f10x_gpio.h>
#include <stm32f10x_i2c.h>
#include <stm32f10x_exti.h>
#include <stm32f10x_dma.h>
#include "misc.h"
#include "stm32f10x_usart.h"
#include <stdio.h>
#include "stm32f10x_tim.h"

#include "n3310.h"
#include "encoder.h"
#include "mpu6050.h"
#include "i2c.h"

u8 ButtDiscavery(void);        // ������ Discavery
u8 ButtEncoder(void);          // ������ ��������
void GPIO_DiscaveryInit(void); // ����������� ������ � LED �� Discavery
void RCC_Init(void);           // ����������� ������� ������������ ����
void NMI_Handler(void);        // ���������� NMI ���������� ��� ���� HSE.
void DMA_SET(void);            // ����������� DMA ��� I2C1 �� ������ ������ �� �����
void EXIT2_interuptInit(void); // ����������� ���������� �� ������� ���� PORTD2
void ResetDMA(void);           // ���������� DMA
void SetupUSART(void);         // ����������� USART1
void WriteByteUART(char data); // �������� ����� � USART1
void WriteDecUART(u16 data);   // �������������� ����� � ������ ���������� ���� � �������� ��� � ����1

DMA_InitTypeDef dma;           // ��������� ��������� ��� ��������� DMA
u8 MPU6050DATA_DMA[15];        // ����� ��� DMA I2C
u16 ACCEL_X;
u8 ACCEL_X1;
u8 ACCEL_X2;
u8 ACCEL_X3;
u8 ACCEL_X4;
u8 ACCEL_X5;
u8 ACCEL_X6;

u16 ACCEL_Y;
u16 ACCEL_Z;
u16 GYRO_X;
u16 GYRO_Y;
u16 GYRO_Z;
u16 TEMP;
u8 count=0;
u8 STEP_I2C=0;                 // ������� ���� ��������/������ i2c
u8 redy_data=0;                // ���� ���������� ������, � ��� ������ ��� ���� �������� � ������� ��������
u16 x;
u8 OPTOsensor =0 ; //��������� ����������� �������
u8 ButtonState =0; //������ ������ ��������, ��� ��������� � ���������� ���
u16 PWMmin = 250;   // ����������� ���


#define MAX_SAMPLES  550    // ������ ������
u16 sample_index=0;            // ����� ������ ������
u8  zero[MAX_SAMPLES];         // ����� ��� ������ ������� ����� ������, ������ ��������� ��� �������
u16 x_osy[MAX_SAMPLES];        // ����� ��� �������� ������ ��� X

void TIM4_IRQHandler(void)
{  //���������� TIM4 ������������ PWM, � ���� ������ ����� �������� ���//
   //******************************************************************//
	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	if(EncoderRead()<PWMmin)EncoderWrite(PWMmin);
	if(ButtonState){TIM4->CCR3 = EncoderRead();}else{TIM4->CCR3 = PWMmin;};
}

void EXTI2_IRQHandler(void)
{  //********���������� EXIT2 �� mpu6050 �� ���������� ������**********//
   //******************************************************************//
     // GPIOC->BSRR |= GPIO_BSRR_BS9;
      ResetDMA();                              // ���������� DMA
      STEP_I2C = 0;                            // ���������� ���� ������� ����� I2C
	  I2C1->CR1 |= I2C_CR1_ACK;                // �������� ACK
	  I2C1->CR1 |= I2C_CR1_START;              // ������������ ������� �����
	  DMA_Cmd(DMA1_Channel7, ENABLE);          // �������� DMA!
      EXTI->PR |= EXTI_PR_PR2;                 // ���������� ���� ����������
     // GPIOC->BRR |= GPIO_BRR_BR9;
}

void EXTI3_IRQHandler(void)
{  //********���������� EXIT3 �� OPTO ********************************//
   //******************************************************************//
     // GPIOC->BSRR |= GPIO_BSRR_BS8;
      OPTOsensor ^= (1<<0);         // ����������� ���
      EXTI->PR |= EXTI_PR_PR2;      // ���������� ���� ����������


}

void DMA1_Channel7_IRQHandler(void)
{	//**********************���������� DMA I2C RX**********************//
	//*****************************************************************//
	// GPIOC->BSRR |= GPIO_BSRR_BS8;
    if(DMA_GetITStatus(DMA1_IT_TC7)==SET) // �������� �� ������� ������������ ���������� �� ���������� ������ I2C1
   {
	DMA_ClearITPendingBit(DMA1_IT_TC7);   // ���������� ���� ����
	I2C1->CR1 &= ~I2C_CR1_ACK;            // ��������� ACK (��� NACK)
	I2C1->CR1 |= I2C_CR1_STOP;            // �������� STOP ��� � I2C
    ACCEL_X = (MPU6050DATA_DMA[1]<<8) | MPU6050DATA_DMA[0];
    ACCEL_Y = (MPU6050DATA_DMA[3]<<8) | MPU6050DATA_DMA[2];
    ACCEL_Z = (MPU6050DATA_DMA[5]<<8) | MPU6050DATA_DMA[4];
    TEMP    = (MPU6050DATA_DMA[7]<<8) | MPU6050DATA_DMA[6];
    GYRO_X  = (MPU6050DATA_DMA[9]<<8) | MPU6050DATA_DMA[8];
    GYRO_Y =  (MPU6050DATA_DMA[11]<<8) | MPU6050DATA_DMA[10];
    GYRO_Z  = (MPU6050DATA_DMA[13]<<8) | MPU6050DATA_DMA[12];
/*
    ACCEL_X1 = MPU6050DATA_DMA[8];
    ACCEL_X2 = MPU6050DATA_DMA[9];
    ACCEL_X3 = MPU6050DATA_DMA[10];
    ACCEL_X4 = MPU6050DATA_DMA[11];
    ACCEL_X5 = MPU6050DATA_DMA[12];
    ACCEL_X6 = MPU6050DATA_DMA[13];
    */
    redy_data = 1;
   }
	// GPIOC->BRR |= GPIO_BRR_BR8;
}

void I2C1_EV_IRQHandler(void)
{   //***************����� ���������� �� I2C***************************//
	//*****************************************************************//
	if(I2C1->SR1 & I2C_SR1_SB)                     // ���������� �� ������������ START
	{
		if(!STEP_I2C) I2C1->DR = MPU6050<<1;       // ���������� ����� ���������� c ������ Wrute
		else{I2C1->DR = (MPU6050<<1)|1;STEP_I2C =0;}// ���������� ����� ���������� c ������ Read
		(void) I2C1->SR1;                          // ������� ����
	}
	if(I2C1->SR1 & I2C_SR1_ADDR)                   // ���� ������������� �������� ������, ���������� �� ���� �������
	{
		 (void) I2C1->SR1;                         // ���������� ��� ADDR (������� SR1 � SR2):
		 (void) I2C1->SR2;
		 if(!STEP_I2C) I2C1->DR = 0x3B;            // ���������� ����� ��������
	}
	if(I2C1->SR1 & I2C_SR1_BTF)                    // ���� ������������� �������� �����
	{
        if(!STEP_I2C)
        {
		I2C1->CR1 |= I2C_CR1_START;                // ��������� ��������� �����
		STEP_I2C=1;
        }
	}
}

void I2C1_ER_IRQHandler(void)
{   //***************����� ���������� ������ �� I2C********************//
	//*****************************************************************//
}

void NMI_Handler(void)
{   //*************���������� ��� ���� HSE***************************//
    //*********������������� ��������� �� ���������� RC 8���*********//
     if (RCC->CIR & RCC_CIR_CSSF) RCC->CIR|=RCC_CIR_CSSC; //�������� ���� ������� �������� CSS
}

void RCC_Init(void) // ����������� ������� ������������ ����
{
	    //SystemInit(); - � ����� ��� ����������� ��� � ������
		__IO uint32_t StartUpCounter = 0, HSEStatus = 0;
		// ������������  SYSCLK, HCLK, PCLK2 � PCLK1
		// �������� HSE
		RCC->CR |= ((uint32_t)RCC_CR_HSEON);
		/* ���� ���� HSE �� �������� ��� ���������� ���� �� ������ �������*/
		do{    HSEStatus = RCC->CR & RCC_CR_HSERDY;
		       StartUpCounter++;}
		while( (HSEStatus == 0) && (StartUpCounter != HSEStartUp_TimeOut));
		if ( (RCC->CR & RCC_CR_HSERDY) != RESET){HSEStatus = (uint32_t)0x01;}
		else{HSEStatus = (uint32_t)0x00;}
		/* ���� HSE ���������� ��������� */
		if ( HSEStatus == (uint32_t)0x01)
		{
		        // ���������� �������� ��� ���������
		    	/* HCLK = SYSCLK, AHB �������� */
		    	RCC->CFGR |= (uint32_t)RCC_CFGR_HPRE_DIV1; //24���
		    	/* PCLK2 = HCLK, (APB2 - high speed) �������� ����*/   //24���
		    	RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE2_DIV1;
		    	/* PCLK1 = HCLK, (APB1 - Low speed) �������� ���� */
		    	RCC->CFGR |= (uint32_t)RCC_CFGR_PPRE1_DIV4;          // 24���/8 = 6 ���, ��� � ��� ����� i2C c �������� MPU6050
		        /* ������������� ��������� PLL configuration: PLLCLK = HSE * 3 = 24 MHz */
		    	/* ��� �������, ��� ����� �� 8���! */
		    	/* ������� ��������*/
		    	RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_PLLSRC | RCC_CFGR_PLLXTPRE | RCC_CFGR_PLLMULL));
		    	/*������������� ������ � HSE, �������� �3 -*/
		    	RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC |RCC_CFGR_PLLXTPRE_PREDIV1_Div2 | RCC_CFGR_PLLMULL6);
		        /* �������� PLL */
		        RCC->CR |= RCC_CR_PLLON;
		        /* �������, ���� PLL �������� ��� ���������� */
		        while((RCC->CR & RCC_CR_PLLRDY) == 0){}; // ����
		    	/* �������� PLL ��� �������� ��������� ������� */
		    	RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
		        RCC->CFGR |= (uint32_t)RCC_CFGR_SW_PLL;
		    	/* �������, ���� PLL ��������� ��� �������� ��������� ������� */
		    	while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != (uint32_t)0x08){}; // ����
		    	RCC->CR |= RCC_CR_CSSON; // �������� ������ HSE
          }
}

void GPIO_DiscaveryInit(void)
{
	            // init for GPIO (LED Discavery)
		        GPIO_InitTypeDef GPIO_Init_Strc;                                // �������� ���� ��������� ��� ����������� �����
		    	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE); 		    // ������������� ������������ ����� �
		    	GPIO_Init_Strc.GPIO_Speed = GPIO_Speed_50MHz;     		    	// �������� �����
		    	GPIO_Init_Strc.GPIO_Mode  = GPIO_Mode_Out_PP;    		        // �����  ��� ��� (���� 0 ���� 1)
		    	GPIO_Init_Strc.GPIO_Pin   = GPIO_Pin_8 | GPIO_Pin_9 ;           // two LED (guess on what pin!!)
		    	GPIO_Init(GPIOC, &GPIO_Init_Strc);
	        /*    // init for GPIO (BUTTON DISCAVERY)
		    	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 		    // ������������� ������������ ����� B
		    	GPIO_Init_Strc.GPIO_Speed = GPIO_Speed_2MHz;     		    	// �������� �����
		    	GPIO_Init_Strc.GPIO_Mode  = GPIO_Mode_IPU;  		            // �����  ����� � ��������� � �����
		    	GPIO_Init_Strc.GPIO_Pin   = GPIO_Pin_0 ;
		    	GPIO_Init(GPIOA, &GPIO_Init_Strc);
		    */	// init for GPIO (BUTTON ENCODER)
		        RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE); 		    // ������������� ������������ ����� B
		    	GPIO_Init_Strc.GPIO_Speed = GPIO_Speed_2MHz;     		    	// �������� �����
		    	GPIO_Init_Strc.GPIO_Mode  = GPIO_Mode_IPU;  		            // �����  ���� � ��������� � �������
		    	GPIO_Init_Strc.GPIO_Pin   = GPIO_Pin_1 ;
		    	GPIO_Init(GPIOB, &GPIO_Init_Strc);
}

u8 ButtDiscavery(void) // ������ Discavery
{	u8 press = GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0);
	return press;}
u8 ButtEncoder(void)   // ������ ��������
{	u8 press = GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1);
	press ^= 1;        // ����������� ���, �.�. ��� ������� = 0;
	return press;}

void DMA_SET(void)
{	//**********************************************************************//
	//*************��������� DMD I2C1 �� ������ ������**********************//

                       RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);         // ������������� DMA1
	                   DMA_DeInit(DMA1_Channel7);                                 // ���������� DMA - ����������� !!!
	   		    	   DMA_StructInit(&dma);                                      // ������� ���������
	   		    	   dma.DMA_PeripheralBaseAddr = (uint32_t)&(I2C1->DR);        // ����� ������������� ����������
	   		    	   dma.DMA_MemoryBaseAddr = (uint32_t)&MPU6050DATA_DMA[0];    // ����� ������� ������ ������
	   		    	   dma.DMA_DIR = DMA_DIR_PeripheralSRC;                       // ��������� - ��������
	   		    	   dma.DMA_BufferSize = 13;                                   // ������ ������ - 13  (0..13) = 14 ����� !
	   		    	   dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;         // ��������� ������ ���������: ��������
	   		    	   dma.DMA_MemoryInc = DMA_MemoryInc_Enable;                  // ��������� ������ ������: ��������
	   		    	   dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;  // ������ �� ��������� ������� (u8)
	   		    	   dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;          //
	   		    	   dma.DMA_Priority = DMA_Priority_High ;                     // ��������� ������
	   		    	   dma.DMA_M2M = DMA_M2M_Disable ;                            // �������� ������<->������  DMA_memory_to_memory
	   		    	   dma.DMA_Mode = DMA_Mode_Normal;
	   		    	   DMA_Init(DMA1_Channel7, &dma);                             // i2c1 RX - 7 �����

				       I2C_DMACmd(I2C1,ENABLE);                        // ��������� ������  I2C1 � DMA
		   		       NVIC_EnableIRQ(DMA1_Channel7_IRQn);             // ��������� ���������� ���������� �� ������ 7
		   		       DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE); // ��������� ��������� ���������� �� ���������� ������
			           __enable_irq();                                 // ���������� ����� �� ����������
/*
 * ����� ������������� ������ DMA ���� ��������� :
 * ������� �������� -                    DMA1_Channel7->CCR = 0;
 * �������� ���� ������ ����������� -    DMA1_Channel7->CCR = 0x00003083;
 * �������� ��������� ���������� ������  DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE);
 * �������� ������� ���� -               DMA1_Channel7->CNDTR = 13; //���������� ������������ ��������
 * ��� �������� ������������ � ����������� ����������, ��� �� ������������� DMA
 */}


void ResetDMA(void) // ������������ DMA
{	//**********************************************************************//
	//*************������������ DMA ��� ������������ ������*****************//
/*	 DMA1_Channel7->CCR = 0;
	 // DMA1_Channel7->CCR  |= (DMA_CCR1_MINC | DMA_CCR1_PL_0);
	 DMA1_Channel7->CCR = 0x00003080; // ���� �����
	 DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE); // ��������� ��������� ���������� �� ���������� ������
	 DMA1_Channel7->CNDTR = 13; //���������� ������������ ��������*/
/* ����������� ��������� �� 13.81 ��� ������ */
/********************************************/
	DMA_DeInit(DMA1_Channel7);                                 // ���������� DMA - ����������� !!!
	DMA_StructInit(&dma);                                      // ������� ���������
	dma.DMA_PeripheralBaseAddr = (uint32_t)&(I2C1->DR);        // ����� ������������� ����������
	dma.DMA_MemoryBaseAddr = (uint32_t)&MPU6050DATA_DMA[0];    // ����� ������� ������ ������
	dma.DMA_DIR = DMA_DIR_PeripheralSRC;                       // ��������� - ��������
	dma.DMA_BufferSize = 14;                                   // ������ ������ - 13  (0..13) = 14 ����� !
	dma.DMA_PeripheralInc = DMA_PeripheralInc_Disable;         // ��������� ������ ���������: ��������
	dma.DMA_MemoryInc = DMA_MemoryInc_Enable;                  // ��������� ������ ������: ��������
	dma.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;  // ������ �� ��������� ������� (u8)
	dma.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;          //
	dma.DMA_Priority = DMA_Priority_High ;                     // ��������� ������
	dma.DMA_M2M = DMA_M2M_Disable ;                            // �������� ������<->������ ��������� DMA_memory_to_memory
	dma.DMA_Mode = DMA_Mode_Normal;
	DMA_Init(DMA1_Channel7, &dma);
	DMA_ITConfig(DMA1_Channel7, DMA_IT_TC, ENABLE);            // ��������� ��������� ���������� �� ���������� ������

}



void InteruptInit(void)
{
	/*************��������� ���������� � PORTD 2****************************/
	/* ��� ���������� ������� ������� ������ mpu6050 �� ���������� ������  */
	   		    	 GPIO_EXTILineConfig(GPIO_PortSourceGPIOD, GPIO_PinSource2);
	   		    	 EXTI_InitTypeDef EXIT_strct;
	   		    	 EXIT_strct.EXTI_Line = EXTI_Line2;
	   		    	 EXIT_strct.EXTI_Mode = EXTI_Mode_Interrupt;
	   		    	 EXIT_strct.EXTI_Trigger = EXTI_Trigger_Rising;
	   		    	 EXIT_strct.EXTI_LineCmd =ENABLE;
	   		    	 EXTI_Init(&EXIT_strct);

	   		    	 NVIC_InitTypeDef nvic;
	   		    	 nvic.NVIC_IRQChannel = EXTI2_IRQn;  // ��������� ����� IRQ
	   		    	 nvic.NVIC_IRQChannelPreemptionPriority = 2;  // ��������� ������ ( 0 (����� ������������) - 15)
	   		    	 nvic.NVIC_IRQChannelSubPriority = 2;  // ��������� ���������
	   		    	 nvic.NVIC_IRQChannelCmd = ENABLE;
	   		    	 NVIC_Init(&nvic);
	//********************���������� �� I2C1******************************//
	//*******************************************************************//
	   		    	NVIC_InitTypeDef NVIC_InitStruct;
	   		    	 // ��������� ���������� �� ������ ������
	   		    	NVIC_InitStruct.NVIC_IRQChannel = I2C1_ER_IRQn;
	   		    	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	   		    	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	   		        NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	   		    	NVIC_Init(&NVIC_InitStruct);
	   		    	I2C_ITConfig(I2C1, I2C_IT_ERR, ENABLE); // �������� ���������� �� �������� ERR
	   		    	//��������� ���������� �� ������ EVT
	   		    	NVIC_InitStruct.NVIC_IRQChannel = I2C1_EV_IRQn;
	   		    	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	   		    	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	   		    	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	   		    	NVIC_Init(&NVIC_InitStruct);
	   		    	I2C_ITConfig(I2C1, I2C_IT_EVT, ENABLE);   // �������� ���������� �� �������� EVT
	 /*************��������� ���������� � PORTA3 (OPTO sensor)***************/
	/* ��� ���������� ������� ������� ������ mpu6050 �� ���������� ������  */
	   		 	    GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource3);
                    EXIT_strct.EXTI_Line = EXTI_Line3;
	   		 	   	EXIT_strct.EXTI_Mode = EXTI_Mode_Interrupt;
	   		 	   	EXIT_strct.EXTI_Trigger = EXTI_Trigger_Rising;
	   		 	   	EXIT_strct.EXTI_LineCmd =ENABLE;
	   		 	    EXTI_Init(&EXIT_strct);

	   		 	   	nvic.NVIC_IRQChannel = EXTI3_IRQn;  // ��������� ����� IRQ
	   		 	   	nvic.NVIC_IRQChannelPreemptionPriority = 2;  // ��������� ������ ( 0 (����� ������������) - 15)
	   		 	   	nvic.NVIC_IRQChannelSubPriority = 2;  // ��������� ���������
	   		 	   	nvic.NVIC_IRQChannelCmd = ENABLE;
	   		 	   	NVIC_Init(&nvic);
}


void SetupUSART(void)
{
	 /*������������� GPIOA, USART1                                           */
	  RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1 | RCC_APB2Periph_GPIOA, ENABLE);

      GPIO_InitTypeDef  GPIO_InitStructure;
      USART_InitTypeDef USART_InitStructure;

      /* Enable GPIOA clock                                                   */
      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

      /* Configure USART1 Rx (PA10) as input floating                         */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_10;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* Configure USART1 Tx (PA9) as alternate function push-pull            */
      GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_9;
      GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
      GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_AF_PP;
      GPIO_Init(GPIOA, &GPIO_InitStructure);

      /* USART1 configured as follow:
            - BaudRate = 115200 baud
            - Word Length = 8 Bits
            - One Stop Bit
            - No parity
            - Hardware flow control disabled (RTS and CTS signals)
            - Receive and transmit enabled
            - USART Clock disabled
            - USART CPOL: Clock is active low
            - USART CPHA: Data is captured on the middle
            - USART LastBit: The clock pulse of the last data bit is not output to
                             the SCLK pin
      //USART_OverSampling8Cmd(ENABLE); //����� ��������� ������� �������������
      //USART_OneBitMethodCmd(ENABLE);  //����� ��������� ���������� �������������
      //USART_HalfDuplexCmd (ENABLE);   //����� ������� �������������� �����.
      */
      USART_InitStructure.USART_BaudRate            = 115200;
      USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
      USART_InitStructure.USART_StopBits            = USART_StopBits_1;
      USART_InitStructure.USART_Parity              = USART_Parity_No ;
      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
      USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
      USART_Init(USART1, &USART_InitStructure);
      USART_Cmd(USART1, ENABLE);
}

void WriteByteUART(char data)     // �������� ����� � UART1//
{
    while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET); // ���� ������������ �������� ������
    USART_SendData(USART1,data); // ������ ����
}

void IntialOPTO(void) // ����������� ���� �� ������� ����� ���������� ������
{
    // init for GPIO (OPTO sensor)
            GPIO_InitTypeDef GPIO_Init_Strc;                                // �������� ���� ��������� ��� ����������� �����
	    	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE); 		    // ������������� ������������ ����� B
	    	GPIO_Init_Strc.GPIO_Speed = GPIO_Speed_2MHz;     		    	// �������� �����
	    	GPIO_Init_Strc.GPIO_Mode  =GPIO_Mode_IN_FLOATING; // GPIO_Mode_IPD;  		            // �����  ����� � ��������� � �����
	    	GPIO_Init_Strc.GPIO_Pin   = GPIO_Pin_3 ;
	    	GPIO_Init(GPIOA, &GPIO_Init_Strc);
}

void InitPWM(void)
{
	   // ��������� PORTB 8 �� ����� PWM, � ���� �������������� ������� TIM4_CH3
	    GPIO_InitTypeDef GPIO_Init_Strc;
	    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	    GPIO_Init_Strc.GPIO_Mode = GPIO_Mode_AF_PP;
	    GPIO_Init_Strc.GPIO_Pin = GPIO_Pin_8;
	    GPIO_Init_Strc.GPIO_Speed = GPIO_Speed_2MHz;
	    GPIO_Init(GPIOB, &GPIO_Init_Strc);


	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	TIM_TimeBaseInitTypeDef timer;
	TIM_OCInitTypeDef timerPWM;

	    timer.TIM_Prescaler = (uint16_t) (SystemCoreClock / 500000) - 1; //50��*2000 = 100000;
	    timer.TIM_Period = 10000;       // ����������� �������� , 1000 - ������ ��� �������� 50��
	    timer.TIM_CounterMode = TIM_CounterMode_Up;
	    TIM_TimeBaseInit(TIM4, &timer);

	    timerPWM.TIM_Pulse = 50; // 0...2000 , ���������� ���
	    timerPWM.TIM_OCMode = TIM_OCMode_PWM1;
	    timerPWM.TIM_OutputState = TIM_OutputState_Enable;
	    timerPWM.TIM_OCPolarity = TIM_OCPolarity_High;
	    TIM_OC3Init(TIM4, &timerPWM);

        TIM_OC4PreloadConfig(TIM4, TIM_OCPreload_Enable); // �������������� ����������� �������
        TIM_ARRPreloadConfig(TIM4, ENABLE);               //
	    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	    TIM_Cmd(TIM4, ENABLE);

	    /* �������� ���������� ������������ �������� */
	    TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	    NVIC_EnableIRQ(TIM4_IRQn);
}

void WriteDecUART_6(unsigned int func)
{  // �������� �������������� �����
u8 k = 0;
if (func>999999) func=999999;
unsigned int n=1000000;
u8 i = 0;
for (; i < 6; i++){
 n=n/10;
u8 tetra= func/n;
 if (tetra>0)  k = 1;
u8 bukva=tetra+0x30; // ��������� ����� � ���
if (k>0|| i==5) WriteByteUART(bukva);
 func=func-tetra*n;
}
}

void main(void)
{

	    RCC_Init();             // ����������� ������� ������������
	    GPIO_DiscaveryInit();   // ��������� ������ ����� ��������� LEDgre,LEDblue,Button.
	    LCD_GPIOInit();         // ����������� GPIO
	    SPIInit();              // ����������� SPI
	    LCDInit();              // ����������� LCD5110
	    LcdClear ();            // ������� �������
 	    EncodeInit();           // ����������� ��������
    	Init_i2c();             // ����������� i2c �� ���������� 12c
   	    Delay_ms(1);            // �������� ����� ������������ mpu6050
   	    u8 UOP = mpu6050Init(); // ����������� mpu6050, ���������� ��� ������
	    InteruptInit();         // ��������� ����������
   		DMA_SET();              // ������������ DMA
   		SetupUSART();           // ����������� USART1
   		IntialOPTO();           // ����������� ���� OPTO sensor
   		InitPWM();
   	    GPIO_DiscaveryInit();   // ��������� ������ ����� ��������� LEDgre,LEDblue,Button.
    	EncoderWrite(PWMmin);




	    while(1)
	      {


	      for (sample_index=0; sample_index < MAX_SAMPLES; sample_index++) // ������ ������������� � ��������� ���� �����.
	        {
              while(!redy_data);                 // ���� ���������� ������
              redy_data = 0;
              x_osy[sample_index]= ACCEL_X;      // ������ �������� ������������� ����� ��� (X)
              if(OPTOsensor){GPIOC->BSRR |= GPIO_BSRR_BS8;}else{GPIOC->BRR |= GPIO_BRR_BR8;}; // ���� OPTO ��������
              zero[sample_index]= OPTOsensor;  // ���������� ��������� ����������
              if(OPTOsensor) OPTOsensor=0;

	        }


	      GPIOC->BSRR |= GPIO_BSRR_BS9;
	      //���������� ��� ��� ���� � UART
	      u16 t=0;
	      for (; t < MAX_SAMPLES; t++)
	      {
           u8 val = zero[t];     // ��� ���������� �������� ��������� ��� ����
           zero[t]=0;
	      x = x_osy[t]>>6;

	      WriteDecUART_6(x);
	      WriteByteUART(0x09); // "\t" - TAB
	      if(val){WriteByteUART(0x31);} else {WriteByteUART(0x30);} // ���� ������=1 �� ���������� � ���� 1, ����� 0
	      WriteByteUART(0x0D); // CR  ������� ������
	      WriteByteUART(0x0A); // LF  ����� ������*
	      }
	      WriteByteUART('E');
	      WriteByteUART('n');
	      WriteByteUART('d');



           //********* ��� ������� ������, ��������/��������� ���********//
           if(ButtEncoder()){ButtonState ^= 1;}





	    	LcdClear ();
	        LcdGotoXYFont(0,0);
	   	    LcdStr ( 1, "X=" );
	   	    LcdString_8(ACCEL_X);
	   	    LcdGotoXYFont(0,1);
	    	LcdStr ( 1, "Y=" );
	   	    LcdString_8(ACCEL_Y);
	   	    LcdGotoXYFont(0,2);
	     	LcdStr ( 1, "Z=" );
	   	    LcdString_8(ACCEL_Z);
/*
	        LcdGotoXYFont(0,3);
	   	    LcdStr ( 1, "X=" );
	   	    LcdString_8(GYRO_X);
	   	    LcdGotoXYFont(0,4);
	    	LcdStr ( 1, "Y=" );
	    	LcdString_8(GYRO_Y);
	   	    LcdGotoXYFont(0,5);
	     	LcdStr ( 1, "Z=" );
	     	LcdString_8(GYRO_Z);

	     	*/
	   	    LcdGotoXYFont(0,4);
	   	    LcdStr ( 1, "PWM=" );
	   	    u16 rt =  EncoderRead();
	   	    LcdString_4(EncoderRead());
	   	    LcdStr ( 1, " S=" );
	   	    LcdString_2(ButtonState);

	   	    LcdUpdate ();

	        // GPIOC->BSRR |= GPIO_BSRR_BS9;
	       //  Delay_ms(20);
	        //  GPIOC->BRR |= GPIO_BRR_BR9;
	        // Delay_ms(500);

	      }


}


