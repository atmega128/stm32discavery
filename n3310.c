
#include "n3310.h"

// ��������� ��������� ������� ��������
 void LcdSend    ( byte data, LcdCmdData cd );
// ���������� ����������
 byte  LcdCache [ LCD_CACHE_SIZE ]; // ��� � ��� 84*48 ��� ��� 504 �����
// ����� �� ��������� ���� �������, � ���� �� ����� ��� ����������,
// ����� �������� ��� ������� ���� ��� ��������� ���������. �����
// ����� ���������� ��� ����� ���� ����� ��������� � ��� �������.
 int   LoWaterMark;   // ������ �������
 int   HiWaterMark;   // ������� �������
 int   LcdCacheIdx;   // ��������� ��� ������ � LcdCache[]
 byte  UpdateLcd;     // ���� ��������� ����




void Delay_ms(uint32_t ms)
{
volatile uint32_t nCount; //���������� ��� �����
RCC_ClocksTypeDef RCC_Clocks; //���������� ��� ���������� ������� �������
RCC_GetClocksFreq (&RCC_Clocks); //��������� ������� �������� �������
nCount=(RCC_Clocks.HCLK_Frequency/10000)*ms; //������������� �� � �����
for (; nCount!=0; nCount--); //������ ������ �����
}


void LcdClear ( void )
{
//    // ������� ���� �������
    int i;
    for ( i = 0; i < LCD_CACHE_SIZE; i++ )
    {
        LcdCache[i] = 0x00;
    }
    // ����������� �� Jakub Lasinski (March 14 2009)
    //memset( LcdCache, 0x00, LCD_CACHE_SIZE );

    // ����� ���������� ������ � ������������ ��������
    LoWaterMark = 0;
    HiWaterMark = LCD_CACHE_SIZE - 1;

    // ��������� ����� ��������� ����
    UpdateLcd = TRUE;
}


void LcdSend(unsigned char dat, unsigned char command)
{
    if (command == 1)
    	DC_Pin_Port->ODR &= ~DC_Pin; // D/C off  (comand)
    else
    	DC_Pin_Port->ODR |= DC_Pin; // D/C on    (data)
    SPI_I2S_SendData(SPI1, dat);  // ��������� ������
    while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) == RESET); // ���, ���� ������ �� ����������
}

void LcdUpdate (void)
{
    int i;

    if ( LoWaterMark < 0 )
        LoWaterMark = 0;
    else if ( LoWaterMark >= LCD_CACHE_SIZE )
        LoWaterMark = LCD_CACHE_SIZE - 1;

    if ( HiWaterMark < 0 )
        HiWaterMark = 0;
    else if ( HiWaterMark >= LCD_CACHE_SIZE )
        HiWaterMark = LCD_CACHE_SIZE - 1;

    #ifdef CHINA_LCD  // �������� ��� ���������� �� �� ������������� ������������

        byte x,y;

        // 102 x 64 - ������ �������������� ���������� ������ ���������� ��, ��� ���
        // ������ ������ ������������ �� ������� �� ������� ����� �� 3 �������.
        // ������� ������� �������� ���� - ������� � ������ ������ y+1, � �����
        // ������� ����� (����� ���� ���� �������, �������� � ������ ������)

        x = LoWaterMark % LCD_X_RES;      // ������������� ��������� ����� x
        LcdSend( 0x80 | x, LCD_CMD );     // ������������ ������ ������� LoWaterMark

        y = LoWaterMark / LCD_X_RES + 1;  // ������������� ��������� ����� y+1
        LcdSend( 0x40 | y, LCD_CMD );     // ������������ ������ ������� LoWaterMark

        for ( i = LoWaterMark; i <= HiWaterMark; i++ )
        {
            // �������� ������ � ����� �������
            LcdSend( LcdCache[i], LCD_DATA );

            x++;                 // ������ ������������ ���������� x, ����� ������� ������� �� ����� ������
            if (x >= LCD_X_RES)  // ���� ����� �� ������, �� ��������� �� ��������� ������ (x=0; y++)
            {
                // ����� ������, ����� ����� ��������� ������ ����� �������������� ������,
                // �������� ���� ��������� ��������� �����, ����� ��� �������� :)
                x=0;
                LcdSend( 0x80, LCD_CMD );
                y++;
                LcdSend( 0x40 | y, LCD_CMD );
            }
        }

        LcdSend( 0x21, LCD_CMD );    // �������� ����������� ����� ������
        LcdSend( 0x45, LCD_CMD );    // �������� �������� �� 5 �������� ����� (������������� ������� �������, �������� � ����������)
        LcdSend( 0x20, LCD_CMD );    // �������� ����������� ����� ������ � �������������� ���������

    #else  // �������� ��� ������������� �������

        // ������������� ��������� ����� � ������������ � LoWaterMark
        LcdSend( 0x80 | ( LoWaterMark % LCD_X_RES ), LCD_CMD );
        LcdSend( 0x40 | ( LoWaterMark / LCD_X_RES ), LCD_CMD );

        // ��������� ����������� ����� ������ �������
        for ( i = LoWaterMark; i <= HiWaterMark; i++ )
        {
            // ��� ������������� ������� �� ����� ������� �� ������� � ������,
            // ����� ������ ��������������� �������� ������
            LcdSend( LcdCache[i], LCD_DATA );
        }

    #endif

    // ����� ���������� ������ � �������
    LoWaterMark = LCD_CACHE_SIZE - 1;
    HiWaterMark = 0;

    // ����� ����� ��������� ����
    UpdateLcd = FALSE;
}

/*
 * ���                   :  LcdContrast
 * ��������              :  ������������� ������������� �������
 * ��������(�)           :  �������� -> �������� �� 0x00 � 0x7F
 * ������������ �������� :  ���
 */
void LcdContrast ( unsigned char contrast )
{
    LcdSend( 0x21, LCD_CMD );              // ����������� ����� ������
    LcdSend( 0x80 | contrast, LCD_CMD );   // ��������� ������ �������������
    LcdSend( 0x20, LCD_CMD );              // ����������� ����� ������, �������������� ���������
}

/*
 * ���                   :  LcdGotoXYFont
 * ��������              :  ������������� ������ � ������� x,y ������������ ������������ ������� ������
 * ��������(�)           :  x,y -> ���������� ����� ������� �������. ��������: 0,0 .. 13,5
 * ������������ �������� :  ������ ������������ �������� � n3310.h
 */
byte LcdGotoXYFont ( byte x, byte y )
{
    // �������� ������
    if( x > 13 || y > 5 ) return OUT_OF_BORDER;

    //  ���������� ���������. ��������� ��� ����� � �������� 504 ����
    LcdCacheIdx = x * 6 + y * 84;
    return OK;
}

/*
 * ���                   :  LcdChr
 * ��������              :  ������� ������ � ������� ������� �������, ����� �������������� ��������� �������
 * ��������(�)           :  size -> ������ ������. ������ enum � n3310.h
 *                          ch   -> ������ ��� ������
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */
byte LcdChr ( LcdFontSize size, byte ch )
{
    byte i, c;
    byte b1, b2;
    int  tmpIdx;

    if ( LcdCacheIdx < LoWaterMark )
    {
        // ��������� ������ �������
        LoWaterMark = LcdCacheIdx;
    }

    if ( (ch >= 0x20) && (ch <= 0x7F) )
    {
        // �������� � ������� ��� �������� ASCII[0x20-0x7F]
        ch -= 32;
    }
    else if ( ch >= 0xC0 )
    {
        // �������� � ������� ��� �������� CP1251[0xC0-0xFF]
        ch -= 96;
    }
    else
    {
        // ��������� ���������� (�� ������ ��� � ������� ��� �������� ������)
        ch = 95;
    }

    if ( size == FONT_1X )
    {
        for ( i = 0; i < 5; i++ )
        {
            // �������� ��� ������� �� ������� � ���
            LcdCache[LcdCacheIdx++] = (FontLookup[ch][i]) << 1;
        }
    }
    else if ( size == FONT_2X )
    {
        tmpIdx = LcdCacheIdx - 84;

        if ( tmpIdx < LoWaterMark )
        {
            LoWaterMark = tmpIdx;
        }

        if ( tmpIdx < 0 ) return OUT_OF_BORDER;

        for ( i = 0; i < 5; i++ )
        {
            // �������� ��� ������� �� ������� � ��������� ����������
            c = (FontLookup[ch][i]) << 1;
            // ����������� ��������
            // ������ �����
            b1 =  (c & 0x01) * 3;
            b1 |= (c & 0x02) * 6;
            b1 |= (c & 0x04) * 12;
            b1 |= (c & 0x08) * 24;

            c >>= 4;
            // ������ �����
            b2 =  (c & 0x01) * 3;
            b2 |= (c & 0x02) * 6;
            b2 |= (c & 0x04) * 12;
            b2 |= (c & 0x08) * 24;

            // �������� ��� ����� � ���
            LcdCache[tmpIdx++] = b1;
            LcdCache[tmpIdx++] = b1;
            LcdCache[tmpIdx + 82] = b2;
            LcdCache[tmpIdx + 83] = b2;
        }

        // ��������� x ���������� �������
        LcdCacheIdx = (LcdCacheIdx + 11) % LCD_CACHE_SIZE;
    }

    if ( LcdCacheIdx > HiWaterMark )
    {
        // ��������� ������� �������
        HiWaterMark = LcdCacheIdx;
    }

    // �������������� ������ ����� ���������
    LcdCache[LcdCacheIdx] = 0x00;
    // ���� �������� ������� ��������� LCD_CACHE_SIZE - 1, ��������� � ������
    if(LcdCacheIdx == (LCD_CACHE_SIZE - 1) )
    {
        LcdCacheIdx = 0;
        return OK_WITH_WRAP;
    }
    // ����� ������ �������������� ���������
    LcdCacheIdx++;
    return OK;
}


/*
 * ���                   :  LcdStr
 * ��������              :  ��� ������� ������������� ��� ������ ������ ������� �������� � RAM
 * ��������(�)           :  size      -> ������ ������. ������ enum � n3310.h
 *                          dataArray -> ������ ���������� ������ ������� ����� ����������
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */

byte LcdStr ( LcdFontSize size, byte dataArray[] )
{
    byte tmpIdx=0;
    byte response;
    while( dataArray[ tmpIdx ] != '\0' )
    {
        // ������� ������
        response = LcdChr( size, dataArray[ tmpIdx ] );
        // �� ����� ����������� ���� ���������� OUT_OF_BORDER,
        // ������ ����� ���������� ������ �� ������ �������
        if( response == OUT_OF_BORDER)
            return OUT_OF_BORDER;
        // ����������� ���������
        tmpIdx++;
    }
    return OK;
}


/*
 * ���                   :  LcdPixel
 * ��������              :  ���������� ������� �� ���������� ����������� (x,y)
 * ��������(�)           :  x,y  -> ���������� ���������� �������
 *                          mode -> Off, On ��� Xor. ������ enum � n3310.h
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */
byte LcdPixel ( byte x, byte y, LcdPixelMode mode )
{
    int  index;
    byte  offset;
    byte  data;

    // ������ �� ������ �� �������
    if ( x >= LCD_X_RES || y >= LCD_Y_RES) return OUT_OF_BORDER;

    // �������� ������� � ��������
    index = ( ( y / 8 ) * 84 ) + x;
    offset  = y - ( ( y / 8 ) * 8 );

    data = LcdCache[ index ];

    // ��������� �����

    // ����� PIXEL_OFF
    if ( mode == PIXEL_OFF )
    {
        data &= ( ~( 0x01 << offset ) );
    }
    // ����� PIXEL_ON
    else if ( mode == PIXEL_ON )
    {
        data |= ( 0x01 << offset );
    }
    // ����� PIXEL_XOR
    else if ( mode  == PIXEL_XOR )
    {
        data ^= ( 0x01 << offset );
    }

    // ������������� ��������� �������� � ���
    LcdCache[ index ] = data;

    if ( index < LoWaterMark )
    {
        // ��������� ������ �������
        LoWaterMark = index;
    }

    if ( index > HiWaterMark )
    {
        // ��������� ������� �������
        HiWaterMark = index;
    }
    return OK;
}


/*
 * ���                   :  LcdLine
 * ��������              :  ������ ����� ����� ����� ������� �� ������� (�������� ����������)
 * ��������(�)           :  x1, y1  -> ���������� ���������� ������ �����
 *                          x2, y2  -> ���������� ���������� ����� �����
 *                          mode    -> Off, On ��� Xor. ������ enum � n3310.h
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */
byte LcdLine ( byte x1, byte y1, byte x2, byte y2, LcdPixelMode mode )
{
    int dx, dy, stepx, stepy, fraction;
    byte response;

    // dy   y2 - y1
    // -- = -------
    // dx   x2 - x1

    dy = y2 - y1;
    dx = x2 - x1;

    // dy �������������
    if ( dy < 0 )
    {
        dy    = -dy;
        stepy = -1;
    }
    else
    {
        stepy = 1;
    }

    // dx �������������
    if ( dx < 0 )
    {
        dx    = -dx;
        stepx = -1;
    }
    else
    {
        stepx = 1;
    }

    dx <<= 1;
    dy <<= 1;

    // ������ ��������� �����
    response = LcdPixel( x1, y1, mode );
    if(response)
        return response;

    // ������ ��������� ����� �� �����
    if ( dx > dy )
    {
        fraction = dy - ( dx >> 1);
        while ( x1 != x2 )
        {
            if ( fraction >= 0 )
            {
                y1 += stepy;
                fraction -= dx;
            }
            x1 += stepx;
            fraction += dy;

            response = LcdPixel( x1, y1, mode );
            if(response)
                return response;

        }
    }
    else
    {
        fraction = dx - ( dy >> 1);
        while ( y1 != y2 )
        {
            if ( fraction >= 0 )
            {
                x1 += stepx;
                fraction -= dy;
            }
            y1 += stepy;
            fraction += dx;

            response = LcdPixel( x1, y1, mode );
            if(response)
                return response;
        }
    }

    // ��������� ����� ��������� ����
    UpdateLcd = TRUE;
    return OK;
}


/*
 * ���                   :  LcdCircle
 * ��������              :  ������ ���������� (�������� ����������)
 * ��������(�)           :  x, y   -> ���������� ���������� ������
 *                          radius -> ������ ����������
 *                          mode   -> Off, On ��� Xor. ������ enum � n3310.h
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */
byte LcdCircle(byte x, byte y, byte radius, LcdPixelMode mode)
{
    signed char xc = 0;
    signed char yc = 0;
    signed char p = 0;

    if ( x >= LCD_X_RES || y >= LCD_Y_RES) return OUT_OF_BORDER;

    yc = radius;
    p = 3 - (radius<<1);
    while (xc <= yc)
    {
        LcdPixel(x + xc, y + yc, mode);
        LcdPixel(x + xc, y - yc, mode);
        LcdPixel(x - xc, y + yc, mode);
        LcdPixel(x - xc, y - yc, mode);
        LcdPixel(x + yc, y + xc, mode);
        LcdPixel(x + yc, y - xc, mode);
        LcdPixel(x - yc, y + xc, mode);
        LcdPixel(x - yc, y - xc, mode);
        if (p < 0) p += (xc++ << 2) + 6;
            else p += ((xc++ - yc--)<<2) + 10;
    }

    // ��������� ����� ��������� ����
    UpdateLcd = TRUE;
    return OK;
}



/*
 * ���                   :  LcdSingleBar
 * ��������              :  ������ ���� ����������� �������������
 * ��������(�)           :  baseX  -> ���������� ���������� x (������ ����� ����)
 *                          baseY  -> ���������� ���������� y (������ ����� ����)
 *                          height -> ������ (� ��������)
 *                          width  -> ������ (� ��������)
 *                          mode   -> Off, On ��� Xor. ������ enum � n3310.h
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */
byte LcdSingleBar ( byte baseX, byte baseY, byte height, byte width, LcdPixelMode mode )
{
    byte tmpIdxX,tmpIdxY,tmp;

    byte response;

    // �������� ������
    if ( ( baseX >= LCD_X_RES) || ( baseY >= LCD_Y_RES) ) return OUT_OF_BORDER;

    if ( height > baseY )
        tmp = 0;
    else
        tmp = baseY - height + 1;

    // ��������� �����
    for ( tmpIdxY = tmp; tmpIdxY <= baseY; tmpIdxY++ )
    {
        for ( tmpIdxX = baseX; tmpIdxX < (baseX + width); tmpIdxX++ )
        {
            response = LcdPixel( tmpIdxX, tmpIdxY, mode );
            if(response)
                return response;

        }
    }

    // ��������� ����� ��������� ����
    UpdateLcd = TRUE;
    return OK;
}



/*
 * ���                   :  LcdBars
 * ��������              :  ������ ������ ����������� ��������������� (� ������ PIXEL_ON)
 * ��������(�)           :  data[]     -> ������ ������� ����� ����������
 *                          numbBars   -> ���������� ���������������
 *                          width      -> ������ (� ��������)
 *                          multiplier -> ��������� ��� ������
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 * ����������            :  ���������� ��������� �������� EMPTY_SPACE_BARS, BAR_X, BAR_Y � n3310.h
 * ������                :  byte example[5] = {1, 2, 3, 4, 5};
 *                          LcdBars(example, 5, 3, 2);
 */
byte LcdBars ( byte data[], byte numbBars, byte width, byte multiplier )
{
    byte b;
    byte tmpIdx = 0;
    byte response;

    for ( b = 0;  b < numbBars ; b++ )
    {
        // ������ �� ������ �� �������
        if ( tmpIdx > LCD_X_RES - 1 ) return OUT_OF_BORDER;

        // ������ �������� x
        tmpIdx = ((width + EMPTY_SPACE_BARS) * b) + BAR_X;

        // ������ ���� �������������
        response = LcdSingleBar( tmpIdx, BAR_Y, data[b] * multiplier, width, PIXEL_ON);
        if(response == OUT_OF_BORDER)
            return response;
    }

    // ��������� ����� ��������� ����
    UpdateLcd = TRUE;
    return OK;
}


/*
 * ���                   :  LcdRect
 * ��������              :  ������ ������������� �������������
 * ��������(�)           :  x1    -> ���������� ���������� x ������ �������� ����
 *                          y1    -> ���������� ���������� y ������ �������� ����
 *                          x2    -> ���������� ���������� x ������� ������� ����
 *                          y2    -> ���������� ���������� y ������� ������� ����
 *                          mode  -> Off, On ��� Xor. ������ enum � n3310.h
 * ������������ �������� :  ������ ������������ �������� � n3310lcd.h
 */
byte LcdRect ( byte x1, byte y1, byte x2, byte y2, LcdPixelMode mode )
{
    byte tmpIdx;

    // �������� ������
    if ( ( x1 >= LCD_X_RES) ||  ( x2 >= LCD_X_RES) || ( y1 >= LCD_Y_RES) || ( y2 >= LCD_Y_RES) )
        return OUT_OF_BORDER;

    if ( ( x2 > x1 ) && ( y2 > y1 ) )
    {
        // ������ �������������� �����
        for ( tmpIdx = x1; tmpIdx <= x2; tmpIdx++ )
        {
            LcdPixel( tmpIdx, y1, mode );
            LcdPixel( tmpIdx, y2, mode );
        }

        // ������ ������������ �����
        for ( tmpIdx = y1; tmpIdx <= y2; tmpIdx++ )
        {
            LcdPixel( x1, tmpIdx, mode );
            LcdPixel( x2, tmpIdx, mode );
        }

        // ��������� ����� ��������� ����
        UpdateLcd = TRUE;
    }
    return OK;
}


/*
 * ���                   :  LcdImage
 * ��������              :  ������ �������� �� ������� ������������ � Flash ROM
 * ��������(�)           :  ��������� �� ������ ��������
 * ������������ �������� :  ���
 */
void LcdImage ( const byte *imageData )
{
    // ������������� ��������� ����
    LcdCacheIdx = 0;
    // � �������� ����
    for ( LcdCacheIdx = 0; LcdCacheIdx < LCD_CACHE_SIZE; LcdCacheIdx++ )
    {
        // �������� ������ �� ������� � ���
        LcdCache[LcdCacheIdx] =  *imageData++;
    }

    // ����������� �� Jakub Lasinski (March 14 2009)
  //  memcpy_P( LcdCache, imageData, LCD_CACHE_SIZE );  // ���� ����� ��� � ����, �� �������� ������ ������ � ������� �����������

    // ����� ���������� ������ � ������������ ��������
    LoWaterMark = 0;
    HiWaterMark = LCD_CACHE_SIZE - 1;

    // ��������� ����� ��������� ����
    UpdateLcd = TRUE;
}

void LcdString_4(unsigned int func){  // �������� �������������� �����
byte k = 0;
if (func>9999) func=9999;
unsigned int n=10000;
byte i = 0;
for (; i < 4; i++){
 n=n/10;
 byte tetra= func/n;
 if (tetra>0)  k = 1;
 byte bukva=tetra+0x30; // ��������� ����� � ���
 if (k>0|| i==3) LcdChr (FONT_1X, bukva);
 else LcdChr (FONT_1X, 0x20);// �������� ������ ����� ���� ������� ����
 func=func-tetra*n;}
}

void LcdString_8(uint32_t func){
byte k = 0;
if (func>99999999) func=99999999;
unsigned int n=100000000;
byte i = 0;
for (; i < 8; i++){
 n=n/10;
 byte tetra= func/n;
 if (tetra>0)  k = 1;
 byte bukva=tetra+0x30; // ��������� ����� � ���
 if (k>0|| i==3) LcdChr (FONT_1X, bukva);
 else LcdChr (FONT_1X, 0x20);// �������� ������ ����� ���� ������� ����
 func=func-tetra*n;}
}

void LcdString_2(unsigned int func){  // �������� ���� ������� �����

if (func>99) func=99;
unsigned int n=100;
byte i = 0;
for ( ; i < 2; i++){
 n=n/10;
 byte tetra= func/n;
 byte bukva=tetra+0x30; // ��������� ����� � ���
    LcdChr (FONT_1X, bukva);
 func=func-tetra*n;}
}

void LcdString_no_nul_2(unsigned int func){  // �������� ���� ������� �����
byte k = 0;
if (func>99) func=99;
unsigned int n=100;
byte i = 0;
for (; i < 2; i++){
 n=n/10;
 byte tetra= func/n;
 if (tetra>0)  k = 1;
 byte bukva=tetra+0x30; // ��������� ����� � ���
 if (k>0|| i==3)
    LcdChr (FONT_1X, bukva);
 else LcdChr (FONT_1X, 0x20);// �������� ������ ����� ���� ������� ����
 func=func-tetra*n;}
}


void SPIInit(void) {
    SPI_InitTypeDef SPIConf;
    // ���������, ��� ���������� �� ������ �������� ������
    SPIConf.SPI_Direction = SPI_Direction_1Line_Tx;
    // ���������, ��� ���� ���������� - Master
    SPIConf.SPI_Mode = SPI_Mode_Master;
    // ���������� ����� �� 8 ��� (=1 ����)
    SPIConf.SPI_DataSize = SPI_DataSize_8b;
    // ����� 00
    SPIConf.SPI_CPOL = SPI_CPOL_Low;
    SPIConf.SPI_CPHA = SPI_CPHA_1Edge;
    // ����� ������������� SS ���������� (���� ������������� ����� ����� ���������� SS)
    SPIConf.SPI_NSS = SPI_NSS_Soft;
    // ��������� �������� ��������
    SPIConf.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    // ������� ������ ������� ����� ����� (�.�. ����� �������)
    SPIConf.SPI_FirstBit = SPI_FirstBit_MSB;
    // ����� ��������� � SPI
    SPI_Init(SPI1, &SPIConf);
    // �������  SPI1
    SPI_Cmd(SPI1, ENABLE);
    // SS = 1
    SPI_NSSInternalSoftwareConfig(SPI1, SPI_NSSInternalSoft_Set);
}

void LCD_GPIOInit(void) {
    // �������� ������������ (=�������) �� ����� A, B � �������� SPI1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_SPI1, ENABLE);
    GPIO_InitTypeDef PORT;
    // ������� ���� ��� ���������
    PORT.GPIO_Pin   = SCK_Pin | MOSI_Pin;
    // ���������� ���������� �������� (������������ �������� ����������� 4 ����� � �������)
    PORT.GPIO_Speed = GPIO_Speed_2MHz;
    // (�����!) ���������� �������������� ���. ����� - ����� "�������������� �������" ���
    PORT.GPIO_Mode  = GPIO_Mode_AF_PP;
    // ��������� ���� � ����� �
    GPIO_Init(SCK_Pin_Port, &PORT);

    // ������� ���� ��� ���������
    PORT.GPIO_Pin   =   DC_Pin  | RST_Pin;
    // ���������� �������� (��� - ��� �������)
    PORT.GPIO_Speed = GPIO_Speed_2MHz;
    // �������������� - �����, �����
    PORT.GPIO_Mode  = GPIO_Mode_Out_PP;
    // ��������� ���� � ����� B
    GPIO_Init(DC_Pin_Port, &PORT);
}

void LCDInit(void) {

    RST_Pin_Port->ODR &= ~RST_Pin; // RESET LCD OFF
    Delay_ms(1);
    RST_Pin_Port->ODR |= RST_Pin;  // RESET LCD ON
    LcdSend(0x21, 1);    // set LCD mode
    LcdSend(0xc8, 1);    // set bias voltage
    LcdSend(0x06, 1);    // temperature correction
    LcdSend(0x13, 1);    // 1:48
    LcdSend(0x20, 1);    // use bias command, vertical
    LcdSend(0x0c, 1);    // set LCD mode,display normally
}
