#ifndef __MPU6050_H
#define __MPU6050_H

#include<stm32f10x_rcc.h>
#include<stm32f10x_gpio.h>
#include<stm32f10x_i2c.h>


#define MPU6050_ADDRESS_AD0_LOW     0x68 // 7-�� ������ ����� mpu6050 � �������� � GND ����� AD0.
#define MPU6050_ADDRESS_AD0_HIGH    0x69 // 7-�� ������ ����� mpu6050 � �������� � VDD ����� AD0.
#define MPU6050                     0x68 // ����� �� �������

#define MPU6050_SELF_TEST_X         0x0D // 7:5bit(XA_TEST[4-2]) 4:0bit(XG_TEST[4-0])
#define MPU6050_SELF_TEST_Y         0x0E // 7:5bit(YA_TEST[4-2]) 4:0bit(YG_TEST[4-0])
#define MPU6050_SELF_TEST_Z         0x0F // 7:5bit(ZA_TEST[4-2]) 4:0bit(ZG_TEST[4-0])
#define MPU6050_SELF_TEST_A         0x10 // 5:4bit(XA_TEST[1-0]) 3:2bit(YA_TEST[1-0]) 1:0bit(ZA_TEST[1-0])

#define MPU6050_SMPLRT_DIV          0x19 // ���� ������� ���������� ������� ������������� ��������� Sample Rate = Gyroscope Output Rate / (1 + SMPLRT_DIV)

//********************************************************************

#define MPU6050_CONFIG                0x1A    // �������� ������������
  //������� ������������� �� FSYNC 3:5bit
  #define EXT_SYNC_DISABLED           0<<3
  #define EXT_SYNC_TEMP_OUT_L         1<<3
  #define EXT_SYNC_GYRO_XOUT_L        2<<3
  #define EXT_SYNC_GYRO_YOUT_L        3<<3
  #define EXT_SYNC_GYRO_ZOUT_L        4<<3
  #define EXT_SYNC_ACCEL_XOUT_L       5<<3
  #define EXT_SYNC_ACCEL_YOUT_L       6<<3
  #define EXT_SYNC_ACCEL_ZOUT_L       7<<3
  //�������� ������ ������ ������ (DLPF) 0:2bit
  #define DLPF_BW_256                 0 // (BW)Bandwidth (���������� �����������)
  #define DLPF_BW_188                 1
  #define DLPF_BW_98                  2
  #define DLPF_BW_42                  3
  #define DLPF_BW_20                  4
  #define DLPF_BW_10                  5
  #define DLPF_BW_5                   6

#define MPU6050_GYRO_CONFIG           0x1B    // ������������ ���������, ���� ����
  //�������� �������� ���������, �������/������� 3:4bit
  #define FS_SEL_250                  0<<3
  #define FS_SEL_500                  1<<3
  #define FS_SEL_1000                 2<<3
  #define FS_SEL_2000                 3<<3
  //���������������� ���������, 5:7bit
  #define XG_ST                       1<<7
  #define YG_ST                       1<<6
  #define ZG_ST                       1<<5

#define MPU6050_ACCEL_CONFIG          0x1C    // ������������ �������������
  //�������� ��������������� �������������, 3:4bit
  #define ACCEL_FS_2      		      0<<3
  #define ACCEL_FS_4       		      1<<3
  #define ACCEL_FS_8         		  2<<3
  #define ACCEL_FS_16        		  3<<3
  //���������������� �������������, 5:7bit
  #define XA_ST                       1<<7
  #define YA_ST                       1<<6
  #define ZA_ST                       1<<5

#define MPU6050_MOT_THR               0x1F   // ����� ����������� ��������, ��������� ����������
#define MPU6050_FIFO_EN               0x23   // ��������� FIFO ������ ���:
  //��������, ��� ����� �������� � �����
  #define TEMP_FIFO_EN                1<<7
  #define XG_FIFO_EN                  1<<6
  #define YG_FIFO_EN                  1<<5
  #define ZG_FIFO_EN                  1<<4
  #define ACCEL_FIFO_EN               1<<3
  #define SLV2_FIFO_EN                1<<2
  #define SLV1_FIFO_EN                1<<1
  #define SLV0_FIFO_EN                1<<0
  #define FIFO_DISABLE                0


#define MPU6050_I2C_MST_CTRL          0x24    // ��������� i2c �������
  // ��������������, ���� � MPU6050 ���������� ������� ��� �������, � ��� MPU6050 �������� ��� ������(�������)
  #define MULT_MST_EN                 1<<7    // ��������� ������ - ������������
  #define WAIT_FOR_ES                 1<<6    // ���������� �������, �� ��������� ������ � ������� ������� ��������
  #define SLV_3_FIFO_EN               1<<5    // �������� ������ � ������� �������� � FIFO
  #define I2C_MST_P_NSR               1<<4    // ����������� ������ � ������� ���������
  //�������� ������� ��� "������"i2c, 0:3bit
  #define CLOCK_DIV_348               0x0
  #define CLOCK_DIV_333               0x1
  #define CLOCK_DIV_320               0x2
  #define CLOCK_DIV_308               0x3
  #define CLOCK_DIV_296               0x4
  #define CLOCK_DIV_286               0x5
  #define CLOCK_DIV_276               0x6
  #define CLOCK_DIV_267               0x7
  #define CLOCK_DIV_258               0x8
  #define CLOCK_DIV_500               0x9
  #define CLOCK_DIV_471               0xA
  #define CLOCK_DIV_444               0xB
  #define CLOCK_DIV_421               0xC
  #define CLOCK_DIV_400               0xD
  #define CLOCK_DIV_381               0xE
  #define CLOCK_DIV_364               0xF

// ��� ���������, ��� ��������� �������� ������� Slave
//-------����� ��������, ��� ������/������-------------
#define MPU6050_I2C_SLV0_ADDR        0x25
  #define I2C_SLV_RW                 0b10000000 // ������(1) ��� ������(0)
  #define I2C_SLV_ADDR               0b01111111 // 7 ��� ����� �������� �������
#define MPU6050_I2C_SLV0_REG         0x26       // ���������� ����� ��������
#define MPU6050_I2C_SLV0_CTRL        0x27
  #define I2C_SLV_EN                 1<<7       // ��������� �������� Slave0
  #define I2C_SLV_DISABLE            0<<7       // ��������� �������� Slave0
  #define I2C_SLV_BYTE_SW            1<<6       // ��������� ������������ ��� ����
  #define I2C_SLV_REG_DIS            1<<5       // 1:(����/��� ������), 0:(����� ��������,����/��� ������).
  #define I2C_SLV_GRP                1<<4       // ������� ����������� ��� ���� � ����� 1:(1_2, 3_4, 5_6..),0:(0_1, 2_3, 4_5..)
  #define I2C_SLV_LEN                0b00001111 // ���������� ���� ��� ��������, ���� 0 �� SLV0 ��������
//------------------------------------------------------
// ���� ������������� ���� ��� �� ���������� � ����������� ���������:
// ��� ���������, ��� ��������� �������� ������� Slave1
#define MPU6050_I2C_SLV1_ADDR        0x28
#define MPU6050_I2C_SLV1_REG         0x29
#define MPU6050_I2C_SLV1_CTRL        0x2A
// ��� ���������, ��� ��������� �������� ������� Slave2
#define MPU6050_I2C_SLV2_ADDR        0x2B
#define MPU6050_I2C_SLV2_REG         0x2C
#define MPU6050_I2C_SLV2_CTRL        0x2D
// ��� ���������, ��� ��������� �������� ������� Slave3
#define MPU6050_I2C_SLV3_ADDR        0x2E
#define MPU6050_I2C_SLV3_REG         0x2F
#define MPU6050_I2C_SLV3_CTRL        0x30
// ��� ���������, ��� ��������� �������� ������� Slave4, ����� ����������� �� ����������
#define MPU6050_I2C_SLV4_ADDR        0x31       // ��� R/W, 7-�� ������ ����� ��������
#define MPU6050_I2C_SLV4_REG         0x32       // ���������� ����� ��������
#define MPU6050_I2C_SLV4_DO          0x33       // ������ ��� ������ � ������� ����������
#define MPU6050_I2C_SLV4_CTRL        0x34
  #define I2C_SLV4_EN                1<<7       // ��������� �������� Slave4
  #define I2C_SLV4_INT_EN            1<<7       // ��������� �������� Slave0
  #define I2C_SLV4_REG_DIS           1<<5       // 1:(����/��� ������), 0:(����� ��������,����/��� ������).
  #define I2C_MST_DLY                0b00001111 // ����������� �������� �������� ������� � ������� ��������� �� ��������� � ������� �������������.
#define MPU6050_I2C_SLV4_DI          0x35       // ������ ����������� �� ��������

#define MPU6050_I2C_MST_STATUS       0x36
//������ ������� ���������� ��������� i2c ����� master
  #define MST_PASS_THROUGH           1<<7 // ���������� �� �������� FSYNC
  #define MST_I2C_SLV4_DONE          1<<6 // ���������� �� ���������� ������ �� Slave4
  #define MST_I2C_LOST_ARB           1<<5 // ���������� ���� ������ �� i2cAUX ����
  #define MST_I2C_SLV4_NACK          1<<4 // ���������� ��� ��������� NACK �� Slave4
  #define MST_I2C_SLV3_NACK          1<<3 // ���������� ��� ��������� NACK �� Slave3
  #define MST_I2C_SLV2_NACK          1<<2 // ���������� ��� ��������� NACK �� Slave2
  #define MST_I2C_SLV1_NACK          1<<1 // ���������� ��� ��������� NACK �� Slave1
  #define MST_I2C_SLV0_NACK          1<<0 // ���������� ��� ��������� NACK �� Slave0
  #define MST_I2C_DISABLE            0    // ���������� ���������

#define MPU6050_INT_PIN_CFG          0x37
//���������� ��������� �������� ���������� �� ������� INT
  #define INT_LEVEL_UP               0<<7 // 0:(�� ������ INT �������� �������"1"), 1:(�� ������ INT �������� �������"0")
  #define INT_LEVEL_DOWN             1<<7 // 1:(�� ������ INT �������� �������"0")
  #define INT_OPEN_PP                0<<6 // 0:(push-Pull)
  #define INT_OPEN_OD                1<<6 // 1:(Open-Drain)
  #define LATCH_INT_50mks            0<<5 // 0:(50���)
  #define LATCH_INT_EN               1<<5 // 1:(������� �������, ���� �� ���������� ���� ����������)
  #define INT_RD_CLEAR_STAT          0<<4 // 0:(���� ���������� ��������� ������ ������� IN_STATUS)
  #define INT_RD_CLEAR_ALL           1<<4 // 1:(���������� ��� ����� ������)
  #define FSYNC_INT_LEVEL_UP         0<<3 // 0:(Fsync interupt = "1")
  #define FSYNC_INT_LEVEL_DOWN       1<<3 // 1:(Fsync interupt = "0")
  #define FSYNC_INT_EN               1<<2 // 1:(���. ����� Fsync ��� ���� ����������)
  #define FSYNC_INT_DISABLE          0<<2 // 0:(����. ����� Fsync ��� ���� ����������)
  #define I2C_BYPASS_EN              1<<1 // 1:(������ ������ � i2cAUX �������)
  #define I2C_BYPASS_DISABLE         0<<1 // 0:(������ ������ � i2cAUX ��������)

#define MPU6050_INT_ENABLE           0x38
  //������� ��������� ���������� ����������
  #define MOT_EN                     1<<6 // ���������� �� ����������� ��������
  #define FIFO_OFLOW_EN              1<<4 // ���������� ��� ������������ FIFO
  #define I2C_MST_INT_EN             1<<3 // ���������� �� i2c master
  #define DATA_RDY_EN                1<<0 // ���������� �� ���������� ������

#define MPU6050_INT_STATUS           0x3A
  //������ ���������� ����������
  #define MOT_INT                    1<<6 // ���������� �� ����������� ��������
  #define FIFO_OFLOW_INT             1<<4 // ���������� ��� ������������ FIFO
  #define I2C_MST_INT_INT            1<<3 // ���������� �� i2c master
  #define DATA_RDY_INT               1<<0 // ���������� �� ���������� ������

#define MPU6050_I2C_MST_DELAY_CTRL   0x67
  // �������� �������� i2c master
  #define DELAY_ES_SHADOW  			 1>>7 // ��������� ������� ������ ��������, ���� ��� ������ �� ��������
  #define I2C_SLV4_DLY_EN  			 1<<4 // ��������� �������� ��� Slave4
  #define I2C_SLV3_DLY_EN  			 1<<3 // ��������� �������� ��� Slave3
  #define I2C_SLV2_DLY_EN  			 1<<2 // ��������� �������� ��� Slave2
  #define I2C_SLV1_DLY_EN   		 1<<1 // ��������� �������� ��� Slave1
  #define I2C_SLV0_DLY_EN  			 1<<0 // ��������� �������� ��� Slave0

#define MPU6050_SIGNAL_PATH_RESET    0x68
  // RESET ��������
  #define GYRO_RESET_BIT             1<<2
  #define ACCEL_RESET_BIT            1<<1
  #define TEMP_RESET_BIT             1<<0

#define MPU6050_MOT_DETECT_CTRL      0x69
  //�������� ��� �������������� �������� 4:5bit, 1LSB = 1ms
  #define ACCEL_ON_DELAY0            0<<4 //defolt 4ms
  #define ACCEL_ON_DELAY1            1<<4 //defolt 4ms+1ms
  #define ACCEL_ON_DELAY2            2<<4 //defolt 4ms+2ms
  #define ACCEL_ON_DELAY3            3<<4 //defolt 4ms+3ms

#define MPU6050_USER_CTRL            0x6A
  // ���������������� ���������
  #define FIFO_EN                    1<<6 // ��������� FIFO ������
  #define I2C_MST_EN                 1<<5 // (1)��������� (i2cAUX-master) (0)���� i2cAUX �������� � ������� i2c
  #define I2C_IF_DIS                 1<<4 // 0 , �� ������������� � MPU6050
  #define FIFO_RESET                 1<<2 // ���������� FIFO �����, ������������� ������� ��� ����� ������
  #define I2C_MST_RESET              1<<1 // ���������� i2cAUX (i2c master)
  #define SIG_COND_RESET             1<<0 // ���������� ��� �������� ��������,�������, ������������� ���������� ��� ����� ������

#define MPU6050_PWR_MGMT_1           0x6B
  // ������� ���������� �������� � ������������ 1
  #define DEVICE_RESET               1<<7 // ������ ����� ���� ��������� �� ���������
  #define SLEEP                      1<<6 // ���������� � ������ �����
  #define CYCLE                      1<<5 // ����������� ����������� �� ��� �� �������� ��(LP_WAKE_CTRL)
  #define TEMP_DIS                   1<<3 // 1: ��������� ������ �����������
  //�������� ������������ ���� 0:2bit
  #define CLKSEL_8MHz               0 // ���������� ��������� �� 8���
  #define CLKSEL_PLL_X              1 // ������� �� ��������� ��� X
  #define CLKSEL_PLL_Y				2 // ������� �� ��������� ��� Y
  #define CLKSEL_PLL_Z				3 // ������� �� ��������� ��� Z
  #define CLKSEL_PLL_EXT32K			4 // ������� ������ �� 32.768 ���
  #define CLKSEL_PLL_EXT19M			5 // ������� ������ �� 19.2 ���
  #define CLKSEL_STOP				7 // ������� ���������, ���������� � ������ �����

#define MPU6050_PWR_MGMT_2          0x6C
// ������� ���������� �������� � ������������ 2
  #define LP_WAKE_CTRL_1Hz          0<<6 // ������ ����� �� ���, ����������� ������� 43���.
  #define LP_WAKE_CTRL_5Hz          1<<6
  #define LP_WAKE_CTRL_20Hz         2<<6
  #define LP_WAKE_CTRL_40Hz         3<<6
  // ���������� ������� ������������� ����
  #define STBY_XA                   5 // 1:(����� �������� ��� ��� XA)
  #define STBY_YA                   4 // 1:(����� �������� ��� ��� XY)
  #define STBY_ZA                   3 // 1:(����� �������� ��� ��� ZA)
  #define STBY_XG                   2 // 1:(����� �������� ��� ��� XG)
  #define STBY_YG                   1 // 1:(����� �������� ��� ��� YG)
  #define STBY_ZG                   0 // 1:(����� �������� ��� ��� ZG)

#define MPU6050_FIFO_COUNTH         0x72 // ������� ���� ��������� � ������
#define MPU6050_FIFO_COUNTL         0x73
#define MPU6050_FIFO_R_W            0x74 // ������ FIFO ������
#define MPU6050_WHO_AM_I            0x75 // ��������� 6 ��� ������ ����� MPU6050, �� ��������� - 0x68

//��������� ��������********************
#define MPU6050_ACCEL_XOUT_H        0x3B
#define MPU6050_ACCEL_XOUT_L        0x3C
#define MPU6050_ACCEL_YOUT_H        0x3D
#define MPU6050_ACCEL_YOUT_L        0x3E
#define MPU6050_ACCEL_ZOUT_H        0x3F
#define MPU6050_ACCEL_ZOUT_L        0x40
#define MPU6050_TEMP_OUT_H          0x41
#define MPU6050_TEMP_OUT_L          0x42
#define MPU6050_GYRO_XOUT_H         0x43
#define MPU6050_GYRO_XOUT_L         0x44
#define MPU6050_GYRO_YOUT_H         0x45
#define MPU6050_GYRO_YOUT_L         0x46
#define MPU6050_GYRO_ZOUT_H         0x47
#define MPU6050_GYRO_ZOUT_L         0x48
//������ ������� ��������***************
#define MPU6050_RA_EXT_SENS_DATA_00 0x49
#define MPU6050_RA_EXT_SENS_DATA_01 0x4A
#define MPU6050_RA_EXT_SENS_DATA_02 0x4B
#define MPU6050_RA_EXT_SENS_DATA_03 0x4C
#define MPU6050_RA_EXT_SENS_DATA_04 0x4D
#define MPU6050_RA_EXT_SENS_DATA_05 0x4E
#define MPU6050_RA_EXT_SENS_DATA_06 0x4F
#define MPU6050_RA_EXT_SENS_DATA_07 0x50
#define MPU6050_RA_EXT_SENS_DATA_08 0x51
#define MPU6050_RA_EXT_SENS_DATA_09 0x52
#define MPU6050_RA_EXT_SENS_DATA_10 0x53
#define MPU6050_RA_EXT_SENS_DATA_11 0x54
#define MPU6050_RA_EXT_SENS_DATA_12 0x55
#define MPU6050_RA_EXT_SENS_DATA_13 0x56
#define MPU6050_RA_EXT_SENS_DATA_14 0x57
#define MPU6050_RA_EXT_SENS_DATA_15 0x58
#define MPU6050_RA_EXT_SENS_DATA_16 0x59
#define MPU6050_RA_EXT_SENS_DATA_17 0x5A
#define MPU6050_RA_EXT_SENS_DATA_18 0x5B
#define MPU6050_RA_EXT_SENS_DATA_19 0x5C
#define MPU6050_RA_EXT_SENS_DATA_20 0x5D
#define MPU6050_RA_EXT_SENS_DATA_21 0x5E
#define MPU6050_RA_EXT_SENS_DATA_22 0x5F
#define MPU6050_RA_EXT_SENS_DATA_23 0x60
//*************************************
#define MPU6050_RA_I2C_SLV0_DO      0x63 // �������� ������ Slave0
#define MPU6050_RA_I2C_SLV1_DO      0x64 // �������� ������ Slave1
#define MPU6050_RA_I2C_SLV2_DO      0x65 // �������� ������ Slave2
#define MPU6050_RA_I2C_SLV3_DO      0x66 // �������� ������ Slave3
//*************************************

u8 mpu6050Init(void);



#endif
