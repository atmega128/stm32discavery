#include "encoder.h"

void EncodeInit(void){
GPIO_InitTypeDef GPIO_Init_Enc; // �������� ���� ��������� ��� ����������� �����

         // init for GPIO (ENCODER)
         RCC_APB2PeriphClockCmd(ENCODER_RCC_PORT, ENABLE); 		         // ������������� ������������ ����� A
     	 GPIO_Init_Enc.GPIO_Speed = GPIO_Speed_10MHz;     		    	 // �������� �����
     	 GPIO_Init_Enc.GPIO_Mode  = GPIO_Mode_IPU;  		             // �����  ������������ � ��������� � �������
     	 GPIO_Init_Enc.GPIO_Pin   = ENCODER_PIN_A | ENCODER_PIN_B ;     // ���������� ���� �������
     	 GPIO_Init(ENCODER_PORT, &GPIO_Init_Enc);


	       	RCC->APB1ENR = RCC_APB1ENR_TIM2EN;                 // ������������� ������ ��� ��������
	       	TIM2->CCER = TIM_CCER_CC1P | TIM_CCER_CC2P;        // �������� �������, �� ����������
	       	TIM2->CCMR1 = TIM_CCMR1_CC1S_0 | TIM_CCMR1_CC2S_0; // ����������� ������������� ��� ������� � ������� �����
	       	TIM2->SMCR = TIM_SMCR_SMS_0 | TIM_SMCR_SMS_1;
	       	TIM2->ARR = 6000;                                  // ������ �����
	       	TIM2->CR1 = TIM_CR1_CEN;                           // ���������� ������ ��������
	     //  	TIM2->CNT = 0;
}

uint16_t EncoderRead(void){
	if(TIM2->CNT > 4500) TIM2->CNT = 0;      // ������������ ������� 0...1000
	if(TIM2->CNT > 4000) TIM2->CNT = 4000;
	uint16_t count = ((uint16_t)TIM2->CNT)/4;
	return count;
}

void EncoderWrite(uint16_t count){
	if(count>1000) count = 1000;
	TIM2->CNT = count*4;
}
